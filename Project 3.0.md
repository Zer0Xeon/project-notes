# Project 3 - IRC Client - Electron
## Task 1
### Basic Setup
- [ ] Setup IRC Connection
- [ ] Setup Basic Electron
- [ ] Setup config files

## Task 2
### Electron and IRC display
- [ ] Basic logging of messages into html in electron. 
- [ ] Colour code text for various functions (PrivMsg, ChannelMSG, Notice, Error)
- [ ] Implement Sending of messages.
- [ ] Implement Start screen

## Task 3
### UI/UX
- [ ] Basic UI Design
- [ ] Dark/Light Mode
- [ ] Full UI

## Task 4
### Future Features
- [ ] Start Screen
- [ ] Settings screen inside app, rather than config.
- [ ] Login storage
- [ ] Multiple Network Support