# Project 4.0 - Twitch Bot
## Task 1
### The Basics
- [x] Setup + Test twitch connection
- [x] Implement Ping-Pong Testing
- [x] Twitch Login JSON
- [x] Twitch API Login JSON
## Task 2
### The Zer0 Features
- [ ] Use twitch API to pull Users teams.
- [ ] (O) Add possible ban command
- [x] User filtering for certain commands (Broadcaster, Mod, VIP, Sub)
- [ ] Add shoutout command
- [ ] Add Help Command
## Task 3
### Extras
- [x] Weather API (Check Zer0's Current Weather) - !weather
