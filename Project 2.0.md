# Project number 2 - Personal Site
## Task 1
- [x] Setup New directory on server
- [x] Setup File Structure (img,css,js,etc)
## Task 2
- [x] Decide on framework (MDL, BS)
- [x] Re-make logo in DotGrid
- [x] Start building Homepage
## Task 3
- [x] Add pages for projects and social
- [ ] Include Current Skills page
- [x] Add buttons for Github, Gitlab, Twitter, Discord, and Twitch
## Task 4
- [ ] Add an if clause to check if i'm live on twitch or not.
- [x] Push to webhost
- [ ] Push to gitlab sans my twitch keys